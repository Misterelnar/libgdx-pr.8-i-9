package com.particles.effects;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class Particles extends ApplicationAdapter implements InputProcessor {
		Texture img;
		TiledMap tiledMap;
		OrthographicCamera camera;

		SpriteBatch batch;
		ParticleEffect pe;
	public void create () {
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		camera = new OrthographicCamera();
		camera.setToOrtho(false,w,h);
		camera.update();
		Gdx.input.setInputProcessor(this);

		//Particles
		batch = new SpriteBatch();
		pe = new ParticleEffect();
		pe.load(Gdx.files.internal("particle.p"),Gdx.files.internal("img"));
		pe.setPosition(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);
		pe.start();
	}

	public void render () {
		Gdx.gl.glClearColor(0.5f, 0.2f, 0.6f, 0.5f);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();

		pe.update(Gdx.graphics.getDeltaTime());
		batch.begin();
		pe.draw(batch);
		batch.end();
		if (pe.isComplete()) pe.reset();

	}
	@Override
	public boolean keyDown(int keycode) {

		if(keycode == Input.Keys.NUM_1)
			pe.scaleEffect(1.0f);
		if(keycode == Input.Keys.NUM_2)
			pe.scaleEffect(2.0f);
		if(keycode == Input.Keys.NUM_3)
			pe.scaleEffect(3.0f);
		if(keycode == Input.Keys.NUM_4)
			pe.scaleEffect(4.0f);
		if(keycode == Input.Keys.NUM_0)
			pe.scaleEffect(0);
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {

		pe.setPosition(screenX,Gdx.graphics.getHeight()-screenY);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
